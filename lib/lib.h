/**
 * @file    lib.h
 * @brief   Brief description
 * @author  Author <author@email.com>
 * @date
 * @remarks Copyright (c) GMRV/URJC. All rights reserved.
 *          Do not distribute without further notice.
 */
#ifndef __LIB_LOG__
#define __LIB_LOG__

#include <cmakecommontemplate/api.h>

namespace lib
{

  class Hello
  {
  public:

    CMAKECOMMONTEMPLATE_API
    void sayHello( void );

  };
} // namespace lib

#endif
