#include "lib.h"
#include <iostream>

namespace lib
{
  void Hello::sayHello( void )
  {
    std::cout << "Hello!" << std::endl;
  }
} // namespace lib
