# Name of the project

## Introduction


## Dependencies

* Required dependencies:
    * dep1
    * dep2
    * dep3

* Optional dependencies:
    * optdep1
    * optdep2


## Building

```bash
git clone https://gitlab.gmrv.es/common/cmakecommontemplate.git
mkdir cmakecommontemplate/build && cd cmakecommontemplate/build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

## Running
